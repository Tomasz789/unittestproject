﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Strings;

namespace Strings.Tests
{
    [TestFixture]
    public class StringHelperTests
    {
        [Test]
        public void StringHelper_CountUnequalThrowsArgumentNullException()
        {
            string text = null;

            Assert.Throws<ArgumentNullException>(() => StringHelper.CountUnequal(text));
        }

        [Test]
        public void StringHelper_CountUnequalThrowsArgumentException()
        {
            string text = string.Empty;

            Assert.Throws<ArgumentException>(() => StringHelper.CountUnequal(text));
        }

        [Test]
        public void CountMaximumRepeatedDigitsThrowsArgumentException()
        {
            string text = string.Empty;

            Assert.Throws<ArgumentException>(() => StringHelper.CountUnequal(text));
        }

        [Test]
        public void CountMaximumRepeatedDigitsThrowsArgumentNullException()
        {
            string text = null;
            
            Assert.Throws<ArgumentNullException>(() => StringHelper.CountMaximumRepeatedDigits(text));
        }

        [Test]
        public void CountMaximumRepeatedLettersThrowsArgumentException()
        {
            string text = string.Empty;

            Assert.Throws<ArgumentException>(() => StringHelper.CountMaximumRepeatedLetters(text));
        }

        [Test]
        public void CountMaximumRepeatedLettersThrowsArgumentNullException()
        {
            string text = null;

            Assert.Throws<ArgumentNullException>(() => StringHelper.CountMaximumRepeatedLetters(text));
        }

        [TestCase("1", ExpectedResult = 0)]
        [TestCase("a21c", ExpectedResult = 0)]
        [TestCase("#---^", ExpectedResult = 0)]
        [TestCase("سگ1", ExpectedResult = 0)]
        [TestCase("--dog#34", ExpectedResult = 0)]
        [TestCase("aaa1", ExpectedResult = 3)]
        [TestCase("apple", ExpectedResult = 2)]
        [TestCase("φεγγάρι", ExpectedResult = 0)]
        [TestCase("a aaa a", ExpectedResult = 5)]

        public int CountMaximumRepeatedLetters_ReturnsNumber(string text)
        {
            return StringHelper.CountMaximumRepeatedLetters(text);
        }

        [TestCase("1111", ExpectedResult = 4)]
        [TestCase("apple2", ExpectedResult = 0)]
        [TestCase("1 aaa a", ExpectedResult = 0)]
        [TestCase("apple224", ExpectedResult = 2)]
        [TestCase("φεγγάρι90", ExpectedResult = 0)]

        public int CountMaximumRepeatedDigits(string text)
        {
            return StringHelper.CountMaximumRepeatedDigits(text);
        }

        [TestCase("1111", ExpectedResult = 0)]
        [TestCase("a aaa a",ExpectedResult = 4)]
        [TestCase("apple2", ExpectedResult = 6)]
        [TestCase("apple224", ExpectedResult = 8)]
        [TestCase("φεγγάρι90", ExpectedResult = 9)]

        public int CountUnequalCharacters(string text)
        {
            return StringHelper.CountUnequal(text);
        }
    }
}
