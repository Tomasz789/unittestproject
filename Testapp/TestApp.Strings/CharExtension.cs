﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Strings
{
    public static class CharExtension
    {
        /// <summary>
        /// Determines whether char 'c' is Latin letter.
        /// </summary>
        /// <param name="c">Current character.</param>
        /// <returns>True if is a Latin letter otherwise false.</returns>
        public static bool IsLatin(this char c)
        {
            return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
        }
    }
}
