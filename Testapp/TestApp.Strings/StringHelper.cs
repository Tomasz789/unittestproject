﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Strings
{
    public static class StringHelper
    {
        /// <summary>
        /// Returns a maximum value of unequal consecutive characters.
        /// </summary>
        /// <param name="text">String representation of text to find maximum.</param>
        /// <returns>Maximum of unequal consecutive chars.</returns>
        /// <exception cref="ArgumentNullException">When the text is null.</exception>
        /// <exception cref="ArgumentException">When text is empty.</exception>
        public static int CountUnequal(string text)
        {
            int cnt = 0;
            

            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            if (text.Length == 0)
            {
                throw new ArgumentException(nameof(text));
            }

            for (int i = 0; i < text.Length; i++)
            {
                int current = 1;

                for (int j = i + 1; j < text.Length; j++)
                {
                    if (text[i] != text[j])
                    {
                        current++;
                    }
                    else
                    {
                        break;
                    }

                    if (current > cnt)
                    {
                        cnt = current;
                    }
                }
            }

            return cnt;
        }

        /// <summary>
        /// Returns a maximum value of consecutive characters which are letters.
        /// </summary>
        /// <param name="text">String representation of text to find maximum.</param>
        /// <returns>Maximum of letter.</returns>
        /// <exception cref="ArgumentNullException">When the text is null.</exception>
        /// <exception cref="ArgumentException">When text is empty.</exception>
        public static int CountMaximumRepeatedLetters(string text)
        {
            int cnt = 0;


            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            if (text.Length == 0)
            {
                throw new ArgumentException(nameof(text));
            }

            for (int i = 0; i < text.Length; i++)
            {
                int current = 1;

                if (!char.IsLetter(text[i]))
                {
                    continue;
                }

                if (!text[i].IsLatin())
                {
                    continue;
                }

                for (int j = i + 1; j < text.Length; j++)
                {
                    if (!char.IsLetter(text[j]))
                    {
                        continue;
                    }

                    if (text[i] == text[j])
                    {
                        current++;
                    }
                    else
                    {
                        break;
                    }

                    if (current > cnt)
                    {
                        cnt = current;
                    }
                }
            }

            return cnt;
        }

        /// <summary>
        /// Returns a maximum value of consecutive digits.
        /// </summary>
        /// <param name="text">String representation of text to find maximum.</param>
        /// <returns>Maximum of digits.</returns>
        /// <exception cref="ArgumentNullException">When the text is null.</exception>
        /// <exception cref="ArgumentException">When text is empty.</exception>
        public static int CountMaximumRepeatedDigits(string text)
        {
            int cnt = 0;


            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            if (text.Length == 0)
            {
                throw new ArgumentException(nameof(text));
            }

            for (int i = 0; i < text.Length; i++)
            {
                int current = 1;

                if (!char.IsDigit(text[i]))
                {
                    continue;
                }

                for (int j = i + 1; j < text.Length; j++)
                {
                    if (!char.IsDigit(text[j]))
                    {
                        continue;
                    }

                    if (text[i] == text[j])
                    {
                        current++;
                    }
                    else
                    {
                        break;
                    }

                    if (current > cnt)
                    {
                        cnt = current;
                    }
                }
            }

            return cnt;
        }
    }
}
