# UnitTestProject


## Description

Project 'UnitTestProject' implements three following features:

1. Takes a sequence of symbols as 'string' representation as arguments from the command line.
2. Determines the maximum number of unequal consecutive characters per line to the console.
3. Calculates the maximum number of consecutive identical letters of the Latin alphabet.
4. Calculates the maximum number of consecutive identical digits.

For all features mentioned above unit tests were written. 
Tests are provided for different cases. Also user's input was tested (whether given sequence is correct)...

![](img/main.JPG)

## Dowlnoad

git remote add origin https://gitlab.com/Tomasz789/unittestproject.git
git branch -M main
git push -uf origin main


